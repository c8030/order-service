package io.codex.service.orderservice.dto;

import io.codex.service.dto.core.BaseDTO;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class OrderLineItemDTO extends BaseDTO {

    private Long id;

    private String skuCode;

    private Integer quantity;

    @Builder
    public OrderLineItemDTO(LocalDateTime createdDateTime, LocalDateTime updatedDateTime, Long id, String skuCode, Integer quantity) {
        super(createdDateTime, updatedDateTime);
        this.id = id;
        this.skuCode = skuCode;
        this.quantity = quantity;
    }
}
