package io.codex.service.orderservice.dto;


import io.codex.service.dto.core.BaseDTO;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;


@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class OrderDTO extends BaseDTO {

    private Long id;

    private Long userId;

    private OrderStatus status;

    private List<OrderLineItemDTO> orderLineItems;

    @Builder(toBuilder = true)
    public OrderDTO(LocalDateTime createdDateTime, LocalDateTime updatedDateTime, Long id, Long userId, OrderStatus status, List<OrderLineItemDTO> orderLineItems) {
        super(createdDateTime, updatedDateTime);
        this.id = id;
        this.userId = userId;
        this.status = status;
        this.orderLineItems = orderLineItems;
    }
}