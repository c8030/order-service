package io.codex.service.orderservice.dto;

public enum OrderStatus {
    CREATED, CANCELLED, CONFIRMED
}
