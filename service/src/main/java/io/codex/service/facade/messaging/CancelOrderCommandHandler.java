package io.codex.service.facade.messaging;

import io.codex.service.business.manager.OrderManager;
import io.codex.service.core.exceptions.base.BaseUncheckedException;
import io.codex.service.orderservice.dto.OrderDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
@ExternalTaskSubscription("COMMAND.CANCEL_ORDER")
public class CancelOrderCommandHandler implements ExternalTaskHandler {

    private final OrderManager orderManager;

    @Override
    public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) {
        log.info("Executing cancel order command...");
        OrderDTO orderDTO =  externalTask.getVariable("payload");

        try{
            OrderDTO cancelledOrder = orderManager.cancelOrder(orderDTO);
            VariableMap variables = Variables.createVariables();
            variables.putValue("payload", cancelledOrder);

            log.info("Order Cancelled: {}", orderDTO);

            externalTaskService.complete(externalTask, variables);
        } catch (BaseUncheckedException e) {
            externalTaskService.handleBpmnError(externalTask, "CONFIRM.ORDER.FAILED", e.getMessage());
        }
    }

    private ObjectValue getObjectValue(Object obj){
        return Variables.objectValue(obj).serializationDataFormat(Variables.SerializationDataFormats.JAVA).create();
    }
}
