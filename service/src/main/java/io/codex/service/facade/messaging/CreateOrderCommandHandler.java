package io.codex.service.facade.messaging;

import io.codex.service.business.manager.OrderManager;
import io.codex.service.core.exceptions.base.BaseUncheckedException;
import io.codex.service.orderservice.dto.OrderDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.ObjectValue;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@RequiredArgsConstructor
@ExternalTaskSubscription("COMMAND.PLACE_ORDER")
public class CreateOrderCommandHandler implements ExternalTaskHandler {

    private final OrderManager orderManager;

    @Override
    public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) {
        log.info("executing create order command");
        OrderDTO orderDTO =  externalTask.getVariable("payload");

        try {
            OrderDTO createdOrder = orderManager.createOrder(orderDTO);

            VariableMap variables = Variables.createVariables();
            variables.putValue("payload", createdOrder);

            log.info("Order Created: {}", createdOrder);
            externalTaskService.complete(externalTask, variables);
        } catch (BaseUncheckedException e){
            externalTaskService.handleBpmnError(externalTask, "CREATING.ORDER.FAILED", e.getMessage());
        }
    }

    private ObjectValue getObjectValue(Object obj){
        return Variables.objectValue(obj).serializationDataFormat(Variables.SerializationDataFormats.JAVA).create();
    }
}
