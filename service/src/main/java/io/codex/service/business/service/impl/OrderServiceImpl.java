package io.codex.service.business.service.impl;

import io.codex.service.business.exceptions.OrderNotFoundException;
import io.codex.service.business.mapper.OrderDTOMapper;
import io.codex.service.business.repository.OrderRepository;
import io.codex.service.business.service.OrderService;
import io.codex.service.orderservice.dto.OrderDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final OrderDTOMapper orderDTOMapper;

    @Override
    public OrderDTO createOrder(final OrderDTO orderDTO) {
        log.info("Creating Order: {}", orderDTO);
        return orderDTOMapper.toDTO(orderRepository.save(orderDTOMapper.toEntity(orderDTO)));
    }

    @Override
    public OrderDTO findById(final Long id) {
        log.info("Finding Order for Id: {}", id);
        return orderDTOMapper.toDTO(orderRepository.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(String.format("Order not found for id:%s", id))));
    }

    @Override
    public OrderDTO updateOrder(final OrderDTO orderDTO) {
        log.info("Updating Order: {}", orderDTO);
        findById(orderDTO.getId());
        return orderDTOMapper.toDTO(orderRepository.save(orderDTOMapper.toEntity(orderDTO)));
    }

}
