package io.codex.service.business.service;

import io.codex.service.orderservice.dto.OrderDTO;

public interface OrderService {

    OrderDTO createOrder(final OrderDTO orderDTO);

    OrderDTO findById(final Long id);

    OrderDTO updateOrder(final OrderDTO orderDTO);

}

