package io.codex.service.business.domain.model;

import io.codex.service.core.dao.entities.base.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity(name = "order_line_items")
public class OrderLineItem extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="sku_code", nullable = false)
    private String skuCode;

    @Column(name="quantity", nullable = false)
    private Integer quantity;

    @Builder
    public OrderLineItem(LocalDateTime createdDateTime, LocalDateTime updatedDateTime, Long id, String skuCode, Integer quantity) {
        super(createdDateTime, updatedDateTime);
        this.id = id;
        this.skuCode = skuCode;
        this.quantity = quantity;
    }
}
