package io.codex.service.business.manager;

import io.codex.service.orderservice.dto.OrderDTO;

public interface OrderManager {

    OrderDTO createOrder(OrderDTO orderDTO);

    OrderDTO cancelOrder(OrderDTO orderDTO);

    OrderDTO confirmOrder(OrderDTO orderDTO);

}
