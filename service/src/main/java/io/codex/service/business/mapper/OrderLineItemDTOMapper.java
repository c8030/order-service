package io.codex.service.business.mapper;

import io.codex.service.business.domain.model.OrderLineItem;
import io.codex.service.orderservice.dto.OrderLineItemDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderLineItemDTOMapper {

    OrderLineItemDTO toDTO(OrderLineItem orderLineItem);

    List<OrderLineItemDTO> toDTOs(List<OrderLineItem> orderLineItems);

    OrderLineItem toEntity(OrderLineItemDTO orderLineItemDTO);

    List<OrderLineItem> toEntities(List<OrderLineItemDTO> orderLineItemDTOS);

}