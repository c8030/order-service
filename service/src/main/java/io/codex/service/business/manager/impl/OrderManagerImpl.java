package io.codex.service.business.manager.impl;

import io.codex.service.business.manager.OrderManager;
import io.codex.service.business.service.OrderService;
import io.codex.service.orderservice.dto.OrderDTO;
import io.codex.service.orderservice.dto.OrderStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrderManagerImpl implements OrderManager {

    private final OrderService orderService;

    @Override
    public OrderDTO createOrder(OrderDTO orderDTO) {
        log.info("Creating Order: {}", orderDTO);
        return orderService.createOrder(orderDTO);
    }

    @Override
    public OrderDTO cancelOrder(OrderDTO orderDTO) {
        log.info("Cancelling Order: {}", orderDTO);
        OrderDTO dbOrder = orderService.findById(orderDTO.getId());
        return orderService.updateOrder(dbOrder.
                toBuilder().
                status(OrderStatus.CANCELLED).build());
    }

    @Override
    public OrderDTO confirmOrder(OrderDTO orderDTO) {
        log.info("Confirming Order: {}", orderDTO);
        OrderDTO dbOrder = orderService.findById(orderDTO.getId());
        return orderService.updateOrder(dbOrder.
                toBuilder().
                status(OrderStatus.CONFIRMED).build());
    }
}
