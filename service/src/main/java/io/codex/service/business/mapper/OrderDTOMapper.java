package io.codex.service.business.mapper;

import io.codex.service.business.domain.model.Order;
import io.codex.service.orderservice.dto.OrderDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderDTOMapper {

    OrderDTO toDTO(Order order);

    List<OrderDTO> toDTOs(List<Order> orders);

    Order toEntity(OrderDTO orderDTO);

    List<Order> toEntities(List<OrderDTO> orderDTOS);

}