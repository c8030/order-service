package io.codex.service.business.domain.model;

import io.codex.service.core.dao.entities.base.BaseEntity;
import io.codex.service.orderservice.dto.OrderStatus;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity(name = "orders")
public class Order extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable = false)
    private OrderStatus status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id", orphanRemoval = true, fetch = FetchType.EAGER)
    private List<OrderLineItem> orderLineItems;

    @Builder
    public Order(@NotNull LocalDateTime createdDateTime, LocalDateTime updatedDateTime, Long id, Long userId, OrderStatus status, List<OrderLineItem> orderLineItems) {
        super(createdDateTime, updatedDateTime);
        this.id = id;
        this.userId = userId;
        this.status = status;
        this.orderLineItems = orderLineItems;
    }

    @PrePersist
    public void setDefaultStatus(){
        this.status = OrderStatus.CREATED;
    }
}
