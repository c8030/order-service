package io.codex.service.business.exceptions;

import io.codex.service.core.exceptions.base.NotFoundException;

public class OrderNotFoundException extends NotFoundException {

    public OrderNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderNotFoundException(String message) {
        super(message);
    }
}